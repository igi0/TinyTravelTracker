/*
    Copyright 2024 Igor Calì <igor.cali0@gmail.com>

    This file is part of Open Travel Tracker.

    Open Travel Tracker is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Open Travel Tracker is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Open Travel Tracker.  If not, see <http://www.gnu.org/licenses/>.

*/

package com.igisw.openlocationtracker;

import static androidx.core.content.ContextCompat.startForegroundService;

import android.content.Context;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.os.Environment;
import android.os.StatFs;
import android.util.Log;

import com.igisw.openlocationtracker.AndroidPreferenceSet.AndroidPreferences;
import com.rareventure.util.BackgroundRunner;
import com.rareventure.util.ReadWriteThreadManager;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Locale;
import java.util.Map;
import java.util.WeakHashMap;

//import org.acra.ACRA;

/**
 * Gps Trailer Globals... The reason we stick everything
 * here is 1) to make it easy to find, and 2) we might
 * have generic things like AndroidPreferenceSet which we
 * need to stick somewhere. 3) We have different apps using
 * the same globals but initialize them in different parts of
 * code
 */
public class GTG {

	public static SQLiteDatabase db;
	
	/**
	 * synchronization rules... only the gtgcachecreator can 
	 * perform transactions on the timmy db
	 */
	public static TimmyDatabase timmyDb;

	public static final ReadWriteThreadManager initRwtm = new ReadWriteThreadManager();

	/**
	 * The timestamp of when the user last did an action where we would want them to
	 * reenter their password normally.
	 * Used by password timeout functionality.
	 */
	public static long lastGtgClosedMS;

	/**
	 * Requirements indicate system services and environment states that need to
	 * be a certain way for an activity to run.
	 * Each requirement has an associated require...() method. These methods should
	 * be run in the same order listed (but some may be skipped if unnecessary)
	 * The main reason we have requirements is that some activities require things that other
	 * activities don't. We want to load in all the requirements for all the back activities
	 * at once, so we have to OR requirements of all back pages together. This is opposed to
	 * having simple "setup" methods to setup the system at once for different system modes. 
	 */
	public enum Requirement
	{
		INITIAL_SETUP,
		PREFS_LOADED(INITIAL_SETUP), 
		NOT_IN_RESTORE(INITIAL_SETUP),
		SDCARD_PRESENT(INITIAL_SETUP),
		
		/**
		 * True if the system has been already installed (the external directory
		 * created, the db created, initial parameters set, etc.)
		 */
		SYSTEM_INSTALLED(PREFS_LOADED,INITIAL_SETUP), 
		DB_READY(PREFS_LOADED,INITIAL_SETUP, SDCARD_PRESENT),
		DECRYPT(SYSTEM_INSTALLED,DB_READY,PREFS_LOADED,INITIAL_SETUP),
		ENCRYPT(SYSTEM_INSTALLED,DB_READY,PREFS_LOADED,INITIAL_SETUP),
		/**
		 * If this is not fulfilled, a password will be requested regardless if we need one to
		 * decrypt or not.
		 * This allows us to keep the system working for background processes (like
		 * GpsTrailerCacheCreator) while still requesting a password if the user did
		 * something that would normally lock the application.
		 * Note, be careful that you don't ask for this as a requirement, and later have the
		 * user navigate to a screen that requires full decryption, because then the user
		 * will have to enter their password again
		 */
		PASSWORD_ENTERED(PREFS_LOADED,INITIAL_SETUP), 
		TIMMY_DB_READY(ENCRYPT, DECRYPT, PREFS_LOADED,INITIAL_SETUP),
		
		;
		
		public final int bit;
		
		public int requiredPriorRequirementsBitmap;
		
		Requirement(Requirement ... requiredPriorRequirements)
		{
			if(ordinal() > Integer.SIZE -1)
				throw new IllegalStateException("too many states");
			
			bit = 1 << ordinal();
			
			for(Requirement r : requiredPriorRequirements)
				requiredPriorRequirementsBitmap = 
				(requiredPriorRequirementsBitmap | r.bit | r.requiredPriorRequirementsBitmap);
		}
		
		/**
		 * bitmap of all required prior requirements and current requirement
		 */
		public int priorAndCurrentBitmap()
		{
			return bit|requiredPriorRequirementsBitmap;
		}

		public void fulfill() {
			fulfilledRequirements = (fulfilledRequirements | bit);
			
		}
		
		public boolean isPriorRequirementsFulfilled()
		{
			return (requiredPriorRequirementsBitmap & (~ fulfilledRequirements)) == 0;
		}

		public void assertPriorRequirementsFulfilled()
		{
			if(!isPriorRequirementsFulfilled())
			{
				throw new IllegalStateException("Prior requirements not fulfilled, "+requiredPriorRequirementsBitmap+
						", got "+fulfilledRequirements);
			}
		}

		public boolean isFulfilled() {
			return (fulfilledRequirements & bit) != 0;
		}

		public boolean isFulfilledAndAssertPriorRequirements() {
			// we make sure we are in write mode so the gps trailer service doesn't interfere
			// with the ui (since they both share the same application space and both
			// need to be initted)
			initRwtm.assertInWriteMode();
			assertPriorRequirementsFulfilled();
			return isFulfilled();
		}
		
		public void reset() {
			fulfilledRequirements = (fulfilledRequirements & (~bit));
		}

		public boolean isOn(int bitmap) {
			return (bitmap & bit) != 0;
		}
	}
	
	/**
	 * When we just start out, none of the requirements are fulfilled
	 */
	public static int fulfilledRequirements = 0;
	
	/**
	 * Initial setup should always be called first
	 */
	public static void requireInitialSetup(Context context, boolean inUi )
	{
		if(Requirement.INITIAL_SETUP.isFulfilledAndAssertPriorRequirements())
			return;

		Requirement.INITIAL_SETUP.fulfill();
	}
	
	
	public static boolean requireNotInRestore()
	{
		if(Requirement.NOT_IN_RESTORE.isFulfilledAndAssertPriorRequirements())
			return true;

		if(GTG.GTGEvent.DOING_RESTORE.isOn)
			return false;
		
		Requirement.NOT_IN_RESTORE.fulfill();
		
		return true;
	}
	
	public static void requirePrefsLoaded(Context context)
	{
		if(Requirement.PREFS_LOADED.isFulfilledAndAssertPriorRequirements())
			return;

		loadPreferences(context);
		
		Requirement.PREFS_LOADED.fulfill();
	}
	
	public static boolean requireSdcardPresent(Context context)
	{
		if(Requirement.SDCARD_PRESENT.isFulfilledAndAssertPriorRequirements())
			return true;

		externalFilesDir =  context.getExternalFilesDir(null);

		if(!sdCardMounted(context))
		{
			return false;
		}

		Requirement.SDCARD_PRESENT.fulfill();
		
		return true;
	}
	
	public static boolean requireSystemInstalled(Context context)
	{
		if(Requirement.SYSTEM_INSTALLED.isFulfilledAndAssertPriorRequirements())
			return true;
		
		if(!prefs.initialSetupCompleted)
			return false;
		
		Requirement.SYSTEM_INSTALLED.fulfill();
		
		return true;
			
	}
	
	public static final int REQUIRE_DB_READY_OK = 0;
	public static final int REQUIRE_DB_READY_DB_DOESNT_EXIST = 1;
	
	/**
	 * Opens the database. 
	 */
	public static int requireDbReady()
	{
		//WARNING make sure to update RestoreGpxBackup if you add anything here
		
		if(Requirement.DB_READY.isFulfilledAndAssertPriorRequirements())
			return REQUIRE_DB_READY_OK;
		
		File dbToUse = GpsTrailerDbProvider.getDbFile(false);
		
		if (!dbToUse.exists()) {
			return REQUIRE_DB_READY_DB_DOESNT_EXIST;
		}
		
//		If db is corrupt we simply throw an exception
//		and treat as an internal error. The reason being we are unsure if 
//		an exception is a temporary occurrence or not. So if we prompt the
//		user to destroy an existing database but seemingly corrupt database
//		it may be a bad idea
		GTG.db = GpsTrailerDbProvider.openDatabase(dbToUse);
		
		Requirement.DB_READY.fulfill();
		
		return REQUIRE_DB_READY_OK;
	}
	
	public static final int REQUIRE_DECRYPT_OK = 0;
	public static final int REQUIRE_DECRYPT_BAD_PASSWORD = 1;
	public static final int REQUIRE_DECRYPT_NEED_PASSWORD = 2;

	/**
	 * Require both encrypt and decrypt requirements.
	 * 
	 * @param password if there is a password, this must be set, otherwise may be null
	 * @return 
	 */
	public static int requireEncryptAndDecrypt(String password)
	{
		if(Requirement.DECRYPT.isFulfilledAndAssertPriorRequirements())
			return REQUIRE_DECRYPT_OK;
		
		
		if(GpsTrailerCrypt.prefs.isNoPassword || password != null)
		{
			if(!GpsTrailerCrypt.initialize(GpsTrailerCrypt.prefs.isNoPassword ? null : password))
			{
				return REQUIRE_DECRYPT_BAD_PASSWORD;
			}

			GTG.userLocationCache = new UserLocationCache();
			GTG.gpsLocDbAccessor = new DbDatastoreAccessor<>(GpsLocationRow.TABLE_INFO);
			GTG.gpsLocCache = new GpsLocationCache(GTG.gpsLocDbAccessor, 10);
			GTG.tztSet = new TimeZoneTimeSet();
			tztSet.loadSet();
		}
		else
			return REQUIRE_DECRYPT_NEED_PASSWORD;
		
		Requirement.DECRYPT.fulfill();
		Requirement.ENCRYPT.fulfill();
		Requirement.PASSWORD_ENTERED.fulfill();
		
		return REQUIRE_DECRYPT_OK;
	}
	
	public static void requireEncrypt()
	{
		if(Requirement.ENCRYPT.isFulfilledAndAssertPriorRequirements())
			return;
		
		//TODO 4 we no longer need app id
		GpsTrailerCrypt.initializeWithoutPassword(MASTER_APP_ID);

		GTG.gpsLocDbAccessor = new DbDatastoreAccessor<>(GpsLocationRow.TABLE_INFO);
		GTG.gpsLocCache = new GpsLocationCache(GTG.gpsLocDbAccessor, 10);
		GTG.tztSet = new TimeZoneTimeSet();
				//co: we can't load the set here because we lack the private key to decrypt it with
		//note that we could still decrypt this if there is no set password, but I'd rather
		//keep the flow the same regardless if the user set the password or not
//		tztSet.loadSet();

		Requirement.ENCRYPT.fulfill();
	}
	
	/**
	 * Require that the password be entered since the last time the app was entered
	 * or there is none. 
	 *
	 * @param password if user entered a password, the password that was entered, otherwise
	 *   should be null
	 * @param lastGtgClosedMS
	 */
	public static boolean requirePasswordEntered(String password, long lastGtgClosedMS)
	{
		if(Requirement.PASSWORD_ENTERED.isFulfilledAndAssertPriorRequirements())
			return true;

		boolean status;
		if(GTG.prefs.passwordTimeoutMS != 0 && lastGtgClosedMS + GTG.prefs.passwordTimeoutMS > System.currentTimeMillis())
			status = true;
		else if(password == null)
			status = GpsTrailerCrypt.prefs.isNoPassword;
		else
			status = GpsTrailerCrypt.verifyPassword(password);
		
		if(status)
		{
			Requirement.PASSWORD_ENTERED.fulfill();
		
			return true;
		}
		
		return false;
	}
	
	public static final int REQUIRE_TIMMY_DB_OK = 0;
	public static final int REQUIRE_TIMMY_DB_IS_CORRUPT = 1;
	public static final int REQUIRE_TIMMY_DB_NEEDS_UPGRADING = 2;
	public static final int REQUIRE_TIMMY_DB_NEEDS_PROCESSING_TIME = 3;
	
	public static int requireTimmyDbReady(boolean canWaitAroundAwhile)
	{
		//WARNING!!! If you add anything here, make sure to update shutdownTimmyDb()
		
		if(Requirement.TIMMY_DB_READY.isFulfilledAndAssertPriorRequirements())
			return REQUIRE_TIMMY_DB_OK;
		
		//now setup timmy database, (only for the ui)
		if(timmyDb == null)
		{
			try {
				GTG.timmyDb = GpsTrailerDbProvider.createTimmyDb();
			} catch (IOException e) {
				Log.e(GTG.TAG,"Can't open timmy db", e);
				return REQUIRE_TIMMY_DB_IS_CORRUPT;
			}
		}
		
		//we check if timmy db is open outside of the timmyDb == null if
		// so that if we need more processing time, we don't need to null out timmy db
		// when we go back and try top open the db again
		if(!timmyDb.isOpen())
		{
			try {
				if(timmyDb.isCorrupt())
				{
					return REQUIRE_TIMMY_DB_IS_CORRUPT;
				}
				
				if(!canWaitAroundAwhile && timmyDb.needsProcessingTime())
					return REQUIRE_TIMMY_DB_NEEDS_PROCESSING_TIME;

				timmyDb.open();
	
			} catch (IOException e) {
				throw new IllegalStateException(e);
			}
		}
		
		//we need the database to be open before we mess with properties, so we 
		//check if the timmy db needs upgrade outside of the open if. We also do this
		//because if the user cancels out of the upgrade screen, or hits the home key,
		//this method would be run again, so timmydb would already be open (and not upgraded)
		//which would be bad if we didn't know to upgrade.
		if(timmyDb.isNew())
		{
			timmyDb.setProperty(CACHE_VERSION_NAME, String.valueOf(CACHE_VERSION));
		}
		else if(!GTG.isTimmyDbLatestVersion())
			return REQUIRE_TIMMY_DB_NEEDS_UPGRADING;

		if(apCache == null)
		{
			//note that we create these after open because they need to call
			// getNextRowId() which is only available after the database is opened.
			GTG.apCache = new AreaPanelCache(
					new RollBackTimmyDatastoreAccessor<>(
					GTG.timmyDb.getRollBackTable(GTG.getExternalStorageDirectory() + "/"
							+ GpsTrailerDbProvider.APCACHE_TIMMY_TABLE_FILENAME)));
			GTG.ttCache = new TimeTreeCache(
					new RollBackTimmyDatastoreAccessor<>(
							GTG.timmyDb.getRollBackTable(GTG.getExternalStorageDirectory() + "/"
							+ GpsTrailerDbProvider.TIME_TREE_TIMMY_TABLE_FILENAME)));
			GTG.mediaLocTimeTimmyTable = GTG.timmyDb.getTable(GTG.getExternalStorageDirectory() + "/"
						+ GpsTrailerDbProvider.MEDIA_LOC_TIME_TIMMY_TABLE_FILENAME);

			GTG.cacheCreator = new GpsTrailerCacheCreator();
			
			GTG.mediaLocTimeMap = new MediaLocTimeMap();
			GTG.mediaLocTimeMap.loadFromDb();		
			
			cacheCreator.start();
		}
		
		Requirement.TIMMY_DB_READY.fulfill();
		return REQUIRE_TIMMY_DB_OK;
		
	}

	public static final AndroidPreferenceSet prefSet = new AndroidPreferenceSet() {

		@Override
		public void writePrefs(Map<String, Object> res) {
			res.put("com.rareventure.gps2.GpsTrailerCrypt.Preferences.encryptedPrivateKey",
					GpsTrailerCrypt.prefs.encryptedPrivateKey);
			res.put("com.rareventure.gps2.GpsTrailerCrypt.Preferences.publicKey",
					GpsTrailerCrypt.prefs.publicKey);
			res.put("com.rareventure.gps2.GpsTrailerCrypt.Preferences.salt",
					GpsTrailerCrypt.prefs.salt);
			res.put("com.rareventure.gps2.GpsTrailerCrypt.Preferences.initialWorkPerformed",
					GpsTrailerCrypt.prefs.initialWorkPerformed);
			res.put("com.rareventure.gps2.GpsTrailerCrypt.Preferences.isNoPassword",
					GpsTrailerCrypt.prefs.isNoPassword);
			res.put("com.rareventure.gps2.GpsTrailerCrypt.Preferences.aesKeySize",
					GpsTrailerCrypt.prefs.aesKeySize);

			res.put("com.rareventure.gps2.GTG.Preferences.initialSetupCompleted",
					GTG.prefs.initialSetupCompleted);
			res.put("com.rareventure.gps2.GTG.Preferences.isCollectData",
					GTG.prefs.isCollectData);
			res.put("com.rareventure.gps2.GTG.Preferences.minBatteryPerc",
					GTG.prefs.minBatteryPerc);
			res.put("com.rareventure.gps2.GTG.Preferences.compassData",
					GTG.prefs.compassData);
			res.put("com.rareventure.gps2.GTG.Preferences.useMetric",
					GTG.prefs.useMetric);
			res.put("com.rareventure.gps2.GTG.Preferences.passwordTimeoutMS",
					GTG.prefs.passwordTimeoutMS);
			res.put("com.rareventure.gps2.GTG.Preferences.writeFileLogDebug",
					GTG.prefs.writeFileLogDebug);

			res.put("com.rareventure.gps2.reviewer.map.OsmMapGpsTrailerReviewerMapActivity.Preferences.lastLon",
					OsmMapGpsTrailerReviewerMapActivity.prefs.lastLon);
			res.put("com.rareventure.gps2.reviewer.map.OsmMapGpsTrailerReviewerMapActivity.Preferences.lastLat",
					OsmMapGpsTrailerReviewerMapActivity.prefs.lastLat);
			res.put("com.rareventure.gps2.reviewer.map.OsmMapGpsTrailerReviewerMapActivity.Preferences.lastZoom",
					OsmMapGpsTrailerReviewerMapActivity.prefs.lastZoom);
			res.put("com.rareventure.gps2.reviewer.map.OsmMapGpsTrailerReviewerMapActivity.Preferences.currTimePosSec",
					OsmMapGpsTrailerReviewerMapActivity.prefs.currTimePosSec);
			res.put("com.rareventure.gps2.reviewer.map.OsmMapGpsTrailerReviewerMapActivity.Preferences.currTimePeriodSec",
					OsmMapGpsTrailerReviewerMapActivity.prefs.currTimePeriodSec);
			res.put("com.rareventure.gps2.reviewer.map.OsmMapGpsTrailerReviewerMapActivity.Preferences.showPhotos",
					OsmMapGpsTrailerReviewerMapActivity.prefs.showPhotos);
			res.put("com.rareventure.gps2.reviewer.map.OsmMapGpsTrailerReviewerMapActivity.Preferences.selectedColorRangesBitmap",
					OsmMapGpsTrailerReviewerMapActivity.prefs.selectedColorRangesBitmap);
			res.put("com.rareventure.gps2.reviewer.map.OsmMapGpsTrailerReviewerMapActivity.Preferences.enableToolTips",
					OsmMapGpsTrailerReviewerMapActivity.prefs.enableToolTips);
			res.put("com.rareventure.gps2.reviewer.map.OsmMapGpsTrailerReviewerMapActivity.Preferences.panelScale",
					OsmMapGpsTrailerReviewerMapActivity.prefs.panelScale);

			res.put("com.rareventure.gps2.reviewer.map.OsmMapView.Preferences.mapStyle",
					OsmMapView.prefs.mapStyle);

			res.put("com.rareventure.gps2.GpsTrailerGpsStrategy.Preferences.batteryGpsOnTimePercentage",
					GpsTrailerGpsStrategy.prefs.batteryGpsOnTimePercentage);
			
		}

		@Override
		protected void loadPreference(String name, String value) 
		{
			switch (name) {
				case "com.rareventure.gps2.GpsTrailerCrypt.Preferences.encryptedPrivateKey":
					GpsTrailerCrypt.prefs.encryptedPrivateKey = Util.toByte(value);
					break;
				case "com.rareventure.gps2.GpsTrailerCrypt.Preferences.publicKey":
					GpsTrailerCrypt.prefs.publicKey = Util.toByte(value);
					break;
				case "com.rareventure.gps2.GpsTrailerCrypt.Preferences.salt":
					GpsTrailerCrypt.prefs.salt = Util.toByte(value);
					break;
				case "com.rareventure.gps2.GpsTrailerCrypt.Preferences.initialWorkPerformed":
					GpsTrailerCrypt.prefs.initialWorkPerformed = Boolean.parseBoolean(value);
					break;
				case "com.rareventure.gps2.GpsTrailerCrypt.Preferences.isNoPassword":
					GpsTrailerCrypt.prefs.isNoPassword = Boolean.parseBoolean(value);
					break;
				case "com.rareventure.gps2.GpsTrailerCrypt.Preferences.aesKeySize":
					GpsTrailerCrypt.prefs.aesKeySize = Integer.parseInt(value);
					break;
				case "com.rareventure.gps2.GTG.Preferences.initialSetupCompleted":
					GTG.prefs.initialSetupCompleted = Boolean.parseBoolean(value);
					break;
				case "com.rareventure.gps2.GTG.Preferences.isCollectData":
					GTG.prefs.isCollectData = Boolean.parseBoolean(value);
					break;
				case "com.rareventure.gps2.GTG.Preferences.minBatteryPerc":
					GTG.prefs.minBatteryPerc = Float.parseFloat(value);
					break;
				case "com.rareventure.gps2.GTG.Preferences.compassData":
					GTG.prefs.compassData = Integer.parseInt(value);
					break;
				case "com.rareventure.gps2.GTG.Preferences.useMetric":
					GTG.prefs.useMetric = Boolean.parseBoolean(value);
					break;
				case "com.rareventure.gps2.GTG.Preferences.passwordTimeoutMS":
					GTG.prefs.passwordTimeoutMS = Long.parseLong(value);
					break;
				case "com.rareventure.gps2.GTG.Preferences.writeFileLogDebug":
					GTG.prefs.writeFileLogDebug = Boolean.parseBoolean(value);
					break;
				case "com.rareventure.gps2.reviewer.map.OsmMapGpsTrailerReviewerMapActivity.Preferences.lastLon":
					OsmMapGpsTrailerReviewerMapActivity.prefs.lastLon = Double.parseDouble(value);
					break;
				case "com.rareventure.gps2.reviewer.map.OsmMapGpsTrailerReviewerMapActivity.Preferences.lastLat":
					OsmMapGpsTrailerReviewerMapActivity.prefs.lastLat = Double.parseDouble(value);
					break;
				case "com.rareventure.gps2.reviewer.map.OsmMapGpsTrailerReviewerMapActivity.Preferences.lastZoom":
					OsmMapGpsTrailerReviewerMapActivity.prefs.lastZoom = Float.parseFloat(value);
					break;
				case "com.rareventure.gps2.reviewer.map.OsmMapGpsTrailerReviewerMapActivity.Preferences.currTimePosSec":
					OsmMapGpsTrailerReviewerMapActivity.prefs.currTimePosSec = Integer.parseInt(value);
					break;
				case "com.rareventure.gps2.reviewer.map.OsmMapGpsTrailerReviewerMapActivity.Preferences.currTimePeriodSec":
					OsmMapGpsTrailerReviewerMapActivity.prefs.currTimePeriodSec = Integer.parseInt(value);
					break;
				case "com.rareventure.gps2.reviewer.map.OsmMapGpsTrailerReviewerMapActivity.Preferences.showPhotos":
					OsmMapGpsTrailerReviewerMapActivity.prefs.showPhotos = Boolean.parseBoolean(value);
					break;
				case "com.rareventure.gps2.reviewer.map.OsmMapGpsTrailerReviewerMapActivity.Preferences.selectedColorRangesBitmap":
					OsmMapGpsTrailerReviewerMapActivity.prefs.selectedColorRangesBitmap = Integer.parseInt(value);
					break;
				case "com.rareventure.gps2.reviewer.map.OsmMapGpsTrailerReviewerMapActivity.Preferences.enableToolTips":
					OsmMapGpsTrailerReviewerMapActivity.prefs.enableToolTips = Boolean.parseBoolean(value);
					break;
				case "com.rareventure.gps2.reviewer.map.OsmMapGpsTrailerReviewerMapActivity.Preferences.panelScale":
					OsmMapGpsTrailerReviewerMapActivity.prefs.panelScale = Integer.parseInt(value);
					break;
				case "com.rareventure.gps2.GpsTrailerGpsStrategy.Preferences.batteryGpsOnTimePercentage":
					GpsTrailerGpsStrategy.prefs.batteryGpsOnTimePercentage = Float.parseFloat(value);
					break;
				case "com.rareventure.gps2.reviewer.map.OsmMapView.Preferences.mapStyle":
					OsmMapView.prefs.mapStyle = Enum.valueOf(OsmMapView.Preferences.MapStyle.class, value);
					break;
				default:
					Log.e(GTG.TAG, "ignoring pref: " + name);
					break;
			}
		}};
	public static GpsTrailerCrypt crypt;
	
	//WARNING if you add another cache type, be sure to update lockGpsCaches
	public static AreaPanelCache apCache;
	public static TimeTreeCache ttCache;
	public static GpsLocationCache gpsLocCache;
	public static TimeZoneTimeSet tztSet;
	public static UserLocationCache userLocationCache;
	
	/**
	 * Use for debugging purposes only
	 */
	public static SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd hh:mm:ss", Locale.US);
	
	public static final boolean CLEAR_OUT = false;
	public static final int HACK_FAIL_STOP = Integer.MAX_VALUE;
	public static final boolean DEBUG_SHOW_AREA_PANELS = false;
//	public static final boolean COMMIT_TO_BEFORE_FAILURE = true;
//	public static final boolean CHECK2 = false;
	
//	public static final boolean FREE_VERSION = false;
	
//	public static final boolean START_MAIN_APP = false;
	
	public static boolean HACK_MAKE_TT_CORRUPT = false;
	
	public static final boolean HACK_TURN_OFF_APCACHE_LOADING = false;
	
	public static final Preferences prefs = new Preferences();
	
	/**
	 * A hack to communicate between activities
	 */
	public static GTGAction lastSuccessfulAction;
	
	public enum GTGAction { SET_FROM_AND_TO_DATES, TOOL_TIP_CLICKED }
	
	/**
	 * This lock is for the GpsTrailerCacheCreator. It prevents
	 * (ui) threads that read the cache to find points from
	 * interfering with writing to the cache. Since multiple threads
	 * commonly access the cache at the same time, we don't use
	 * regular synchronization, but instead this a ReadWriteThreadManager,
	 * which allows multiple readers at the same time.
	 */
	public static ReadWriteThreadManager cacheCreatorLock = new ReadWriteThreadManager();

	/**
	 * Named of default SharedPreferences object (persistent store)
	 */
	public static final String SHARED_PREFS_NAME = "GpsPrefs";
	
	/**
	 * ID for frog style notification pop up (for collecting data)
	 */
	public static final int FROG_NOTIFICATION_ID = -42;
	
	/**
	 * Signifies the user_data_key row contains key of the master application (the one which knows the password)
	 * All other keys will be removed and replaced by encrypting with this one. 
	 * (we have several keys because everytime we encrypt, we use a different key, signed by the master public key,
	 * this is how we prevent the app from asking for a password on boot, yet still have everything encrypted)
	 * 
	 */
	public static final int MASTER_APP_ID = 0;
	
//	public static final int SETTINGS_APP_ID = 3;
//	public static final int GPS_TRAILER_SERVICE_APP_ID = 99;

	/**
	 * True if we requested the user let us use gps, and they said no
	 */
	public static boolean userDoesntWantUsToHaveGpsPerm;

	//TODO 2.01 add instrumentation and automatic error reporting
	
	public enum GTGEvent {
		ERROR_LOW_FREE_SPACE,
		ERROR_SDCARD_NOT_MOUNTED,
		ERROR_LOW_BATTERY,
		ERROR_GPS_DISABLED,
		ERROR_GPS_NO_PERMISSION,

		ERROR_SERVICE_INTERNAL_ERROR,
		TTT_SERVER_DOWN,
	 	ERROR_UNLICENSED, PROCESSING_GPS_POINTS, LOADING_MEDIA, DOING_RESTORE ;
								
		public boolean isOn;
		public Object obj;
	}

	public interface GTGEventListener
	{
		/**
		 * Note that this will always be run in the UI thread
		 * 
		 * @return true if the event has been handled and can be turned off.
		 */
		boolean onGTGEvent(GTGEvent event);

		void offGTGEvent(GTGEvent event);
	}

	public static final long MIN_FREE_SPACE_ON_SDCARD = 50L * 1024 * 1024; //WARNING: this is hardcoded into R.string.error_low_free_space,
	//if updated, update there as well

	/**
	 * If there is a serious problem that would prevent the application from running
	 * successfully, this will create an alert and return true.
	 * Otherwise if things are good to go, it returns false
	 */
	public static boolean checkSdCard(Context context)
	{
		if(!sdCardMounted(context))
		{
			GTG.alert( GTGEvent.ERROR_SDCARD_NOT_MOUNTED);
			return false;
		}
		
		StatFs stat = new StatFs(Environment.getExternalStorageDirectory().getPath());
		long sdAvailSize = stat.getAvailableBlocksLong() * stat.getBlockSizeLong();
		
		if(sdAvailSize < MIN_FREE_SPACE_ON_SDCARD)
		{
			GTG.alert( GTGEvent.ERROR_LOW_FREE_SPACE);
			return false;
		}
		
		return true;
		
	}
	
	public static boolean sdCardMounted(Context context) {
		String state = Environment.getExternalStorageState();
		
		//sometimes even though the external media is mounted, the external storage directory may not be present
		//for some reason
		return state.equals(Environment.MEDIA_MOUNTED) && (context == null ||  getExternalStorageDirectory() != null);
	}

	public static DbDatastoreAccessor<GpsLocationRow> gpsLocDbAccessor;

	/**
	 * A background process for loading data into the areapanel and timetree
	 * caches. We make it this way because it takes too long to commit 
	 * to the database to properly quit, so it often needs to run in the 
	 * background even when the user thinks they have exited the program
	 * Also handles populating mediaLocTimeMap and updating it to reflect
	 * areapanels
	 * Synchronization rules:
	 * any change to viewnodes, or areapanels must be synchronized against this
	 * TODO 3 possibly change this to synchronize against a more appropriate
	 * object, since cacheCreator is becoming a generic garbage man
	 */
	public static GpsTrailerCacheCreator cacheCreator;

	public static void notifyCollectDataServiceOfUpdate(Context context)
	{
		if(prefs.isCollectData)
			startForegroundService(context,new Intent(context,
                GpsTrailerService.class));
		else
			context.stopService(new Intent(context,
	                GpsTrailerService.class));
	}
	
	private static final ArrayList<GTGEventListener> localEventListeners = new ArrayList<>();
	
	/**
	 * Called when a major event has occurred, such as an internal error.
	 * Is thread safe. Cannot be called before setupIfNecessary() is run.
	 * <p>
	 * This is used to notify the application of a specific condition
	 * with the environment. Note that events are sticky
	 * until handled. This can be used for two purposes, one is to 
	 * ignore an event until a screen comes up that can handle it. The
	 * other is to display a constant message to the user and not 
	 * remove it until the condition is lifted (such as for low
	 * free space). For the second case, the boolean isOn can
	 * turn off an event.
	 * <p> 
	 * Note that thread handling must be handled by the listener
	 * 
	 * @param obj object to set into event. If set, alert will always run, otherwise
	 *   it will be ignored if the event is already on
	 */
	public static void alert(final GTGEvent event, final boolean isOn, final Object obj) {
		int i;

//		Log.d(GTG.TAG,"GTGAlert: event: "+event+", isOn: "+isOn);

		synchronized(eventListeners)
		{
			if(event.isOn == isOn && obj == null) {
//				Log.d(GTG.TAG,"GTGAlert: event ignored");
				return;
			}

			event.isOn = isOn;
			event.obj = obj;
			
			localEventListeners.clear();
			localEventListeners.addAll(eventListeners.keySet());
			i = localEventListeners.size()-1;

//			Log.d(GTG.TAG,"GTGAlert: num listeners "+(i+1));
		}

		for(; i >= 0; i--)
		{
			GTGEventListener el;
			
			el = localEventListeners.get(i);
			
			if(event.isOn)
			{
				if(el.onGTGEvent(event))
				{
					//if the event handler turned off the event, alert everyone that its off
					//and restart
					alert(event, false);
					break;
				}
			}
			else
				el.offGTGEvent(event);
		}
		
		//make sure to clear so that we don't have a memory leak (eventListeners is a WeakHashMap)
		localEventListeners.clear();
	}
	
	
	public static void alert(GTGEvent event,
			boolean isOn) {
		alert(event, isOn, null);
	}
	
	public static void alert(final GTGEvent event) {
		alert(event, true, null);
	}
	
	private static final String CACHE_VERSION_NAME = "CACHE_VERSION";

	private static final int CACHE_VERSION = 1;

	public static boolean isTimmyDbLatestVersion() {
		
		int version = timmyDb.getIntProperty(CACHE_VERSION_NAME,0);
		return version == CACHE_VERSION; // && 1 == 0; //xODO 2 HACK
	}

	public static final String TAG = "GpsTrailer";

	public static final int GPS_TRAILER_OVERLAY_DRAWER_PRIORITY = 5;

	public static final int REQUIREMENTS_ENTER_PASSWORD =
			Requirement.INITIAL_SETUP.priorAndCurrentBitmap()
			|Requirement.PREFS_LOADED.priorAndCurrentBitmap()
			;

	public static final int REQUIREMENTS_FATAL_ERROR =  Requirement.INITIAL_SETUP.priorAndCurrentBitmap();
	
	public static final int REQUIREMENTS_BASIC_UI = 
			Requirement.INITIAL_SETUP.priorAndCurrentBitmap()
			|Requirement.NOT_IN_RESTORE.priorAndCurrentBitmap()
			//|Requirement.NOT_TRIAL_EXPIRED.priorAndCurrentBitmap()
			//|Requirement.NOT_TRIAL_WHEN_PREMIUM_IS_AVAILABLE.priorAndCurrentBitmap()
			;
	
	public static final int REQUIREMENTS_BASIC_PASSWORD_PROTECTED_UI = 
			Requirement.INITIAL_SETUP.priorAndCurrentBitmap()
			|Requirement.NOT_IN_RESTORE.priorAndCurrentBitmap()
			//|Requirement.NOT_TRIAL_EXPIRED.priorAndCurrentBitmap()
			//|Requirement.NOT_TRIAL_WHEN_PREMIUM_IS_AVAILABLE.priorAndCurrentBitmap()
			|Requirement.DECRYPT.priorAndCurrentBitmap()
			|Requirement.ENCRYPT.priorAndCurrentBitmap()
			|Requirement.PASSWORD_ENTERED.priorAndCurrentBitmap();
			
	public static final int REQUIREMENTS_DB_DOESNT_EXIST_PAGE = 
			Requirement.INITIAL_SETUP.priorAndCurrentBitmap()
			//|Requirement.NOT_TRIAL_EXPIRED.priorAndCurrentBitmap()
			//|Requirement.NOT_TRIAL_WHEN_PREMIUM_IS_AVAILABLE.priorAndCurrentBitmap()
			;
	//co: although it's a little weird not to ask for a password here, we aren't allowing
	//the user any access to the gps points and if we do ask for a password and then
	//the user navigates to the main screen, they'd have to enter the password again
	// for the decrypt requirement
//			|Requirement.PASSWORD_ENTERED.priorAndCurrentBitmap();
			
			
	public static final int REQUIREMENTS_FULL_PASSWORD_PROTECTED_UI = 
			 Requirement.DB_READY.priorAndCurrentBitmap()
			|Requirement.DECRYPT.priorAndCurrentBitmap()
			|Requirement.ENCRYPT.priorAndCurrentBitmap()
			|Requirement.INITIAL_SETUP.priorAndCurrentBitmap()
			|Requirement.PASSWORD_ENTERED.priorAndCurrentBitmap()
			|Requirement.PREFS_LOADED.priorAndCurrentBitmap()
			|Requirement.SDCARD_PRESENT.priorAndCurrentBitmap()
			|Requirement.SYSTEM_INSTALLED.priorAndCurrentBitmap()
			|Requirement.NOT_IN_RESTORE.priorAndCurrentBitmap()
			|Requirement.TIMMY_DB_READY.priorAndCurrentBitmap();

	/**
	 * Initial setup wizard 
	 */
	public static final int REQUIREMENTS_WIZARD =
			Requirement.INITIAL_SETUP.priorAndCurrentBitmap()
			|Requirement.PREFS_LOADED.priorAndCurrentBitmap();

	public static final int REQUIREMENTS_RESTORE = REQUIREMENTS_FULL_PASSWORD_PROTECTED_UI &
				(~Requirement.NOT_IN_RESTORE.bit);

	//these are the preferences we save.
	
	private static void loadPreferences(Context context) {
		//TODO 3: notify user if an exception is thrown here when the
		// preferences are corrupted. We then should reset to factory settings...
		// don't forget to load the password check field
		GTG.prefSet.loadAndroidPreferences(context);
		
		//colorRanges only gets updated from this method (it is an array, so that's why it's
		// not stored in prefs directly)
		OsmMapGpsTrailerReviewerMapActivity.prefs.updateColorRangeBitmap(OsmMapGpsTrailerReviewerMapActivity.prefs.selectedColorRangesBitmap);
	}
	
	//note must be threadsafe for TTTClient
	//TODO 3 get rid of this method
	public static void savePreferences(Context context) {
		GTG.prefSet.savePrefs(context);
	}
	
	public static void runBackgroundTask(Runnable r)
	{
		if(backgroundRunner == null)
		{
			backgroundRunner = new BackgroundRunner();
			backgroundRunner.setDaemon( true);
			backgroundRunner.start();
		}
		backgroundRunner.addRunnable(r);
	}
	
	private static BackgroundRunner backgroundRunner;

	/**
	 * 
	 * Synchronization rules:
	 * any change to rTree or the mlts should synchronize against this
	 */
	public static MediaLocTimeMap mediaLocTimeMap;

	public static TimmyTable mediaLocTimeTimmyTable;

	/**
	 * Everytime the reviewer map is resumed() this value is incremented
	 */
	public static int reviewerMapResumeId;

//	private static Boolean isTrial;

	public static class Preferences implements AndroidPreferences
	{


		/**
		 * Prompts the user to enter initial setup
		 */
		public boolean initialSetupCompleted = false;
		
		/**
		 * If set, gps trailer service will run, otherwise no
		 */
		public boolean isCollectData;

		/**
		 * Minimum amount of battery life before gps collection automatically shuts itself off
		 */
		public float minBatteryPerc = .35f;

		/**
		 * True if we should use metric for scale and stuff
		 */
		public boolean useMetric;

		/**
		 * Shhhh... this is really the date in seconds when the product was installed xor'ed with 
		 * a static value
		 */
		public int compassData = COMPASS_DATA_XOR;

		/**
		 * If not zero, represents the amount of time before the password times out when
		 * not inside the app
		 */
		public long passwordTimeoutMS;

		/**
		 * If true will write to the gps wake lock debug file
		 */
		public boolean writeFileLogDebug = false;
	}
	
	public static final int COMPASS_DATA_XOR = -1016932754;
	
	private static final WeakHashMap<GTGEventListener, Object> eventListeners = new WeakHashMap<>();

	static GpsTrailerService service;

	private static File externalFilesDir;

	public static void addGTGEventListener(
			GTGEventListener eventListener) {
		synchronized(eventListeners)
		{
			if(eventListeners.containsKey(eventListener))
				return;

			for(GTGEvent event : GTGEvent.values()) {
				if(event.isOn)
					eventListener.onGTGEvent(event);
			}

			eventListeners.put(eventListener, Boolean.TRUE);
		}
	}

	public static void removeGTGEventListener(
			GTGEventListener eventListener) {
		synchronized(eventListeners)
		{
			eventListeners.remove(eventListener);
		}
	}



	public static File getExternalStorageDirectory() {
		return externalFilesDir;
	}
	
	public static void setAppPasswordNotEntered() {
		if(Requirement.PASSWORD_ENTERED.isFulfilled())
			GTG.lastGtgClosedMS = System.currentTimeMillis();

		Requirement.PASSWORD_ENTERED.reset();
	}


	/**
	 * Creates an initializes a db (including creating a user data encrypting key. Crypt in preferences must be already set up.
	 * Database is closed after being initialized
	 */
	public static void createAndInitializeNewDbFile() {
		//we create it as a temp file and then move it over so we are sure it 
		//is initialized and ready before we start using it (in case we crash
		// part way through)
		File dbTmpFile = GpsTrailerDbProvider.getDbFile(true);
		
		SQLiteDatabase newDb = GpsTrailerDbProvider.createNewDbFile(dbTmpFile);
		GpsTrailerCrypt.generateAndInitializeNewUserDataEncryptingKey(GTG.MASTER_APP_ID, newDb);
		newDb.close();
		
		dbTmpFile.renameTo(GpsTrailerDbProvider.getDbFile(false));
	}

	/**
	 * Should not be called when gps service is running
	 */
	public static void closeDbAndCrypt() {
		if(GTG.db != null)
		{
			GTG.db.close();
			GTG.db = null;
			Requirement.DB_READY.reset();
		}
		
		if(GTG.crypt != null)
		{
			GTG.crypt = null;
			Requirement.DECRYPT.reset();
			Requirement.ENCRYPT.reset();
		}
	}


	public static void setIsInRestore(boolean b) {
		Requirement.NOT_IN_RESTORE.reset();
		
		GTG.alert(GTGEvent.DOING_RESTORE, b);
	}

	private static final Object shutdownTimmyDbLock = new Object();

	/**
	 * Note this may take awhile if the cache creator is busy. May be interrupted
	 * with interruptShutdownTimmyDb(). 
	 */
	public static void shutdownTimmyDb() {
		if(GTG.timmyDb == null)
			return;
		
		if(GTG.cacheCreator != null)
		{
			Thread shutdownTimmyDbThread;
			synchronized (shutdownTimmyDbLock)
			{
				shutdownTimmyDbThread = Thread.currentThread();
			}
			
			GTG.cacheCreator.shutdown();

			try {
				GTG.cacheCreator.join();
			}
			catch (InterruptedException e) {
				return;
			}
			
			synchronized (shutdownTimmyDbLock)
			{
				shutdownTimmyDbThread = null;
			}
			
			GTG.cacheCreator = null;
		}
		
		GTG.apCache.clear();
		GTG.apCache = null;

		GTG.ttCache.clear();
		GTG.ttCache = null;
		
		GTG.mediaLocTimeMap = null;
		try {
			GTG.timmyDb.close();
		}
		catch (IOException e) {
			throw new IllegalStateException(e);
		}
		GTG.timmyDb = null;
		GTG.mediaLocTimeTimmyTable = null;
		
		Requirement.TIMMY_DB_READY.reset();
	}

	/*
	public static void interruptShutdownTimmyDb()
	{
		synchronized (shutdownTimmyDbLock)
		{
			if(shutdownTimmyDbThread != null)
			{
				shutdownTimmyDbThread.interrupt();
			}
		}
	}
	*/

}
