/**
 Copyright 2024 Igor Calì <igor.cali0@gmail.com>

 This file is part of Open Travel Tracker.

 Open Travel Tracker is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 Open Travel Tracker is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Open Travel Tracker.  If not, see <http://www.gnu.org/licenses/>.

 */

package com.igisw.openlocationtracker;

import android.content.Context;
import android.view.MotionEvent;

import com.mapzen.tangram.MapController;
import com.mapzen.tangram.MapView;
import com.rareventure.gps2.reviewer.map.MyTouchInput;

/**
 * A hack to modify MapController so that we can handle long press + pan
 */
public class MyMapController extends MapController {

    private final Context context;
    private MyTouchInput touchInput;

    protected MyMapController(Context context) {
        super(context);
        this.context = context;
    }

    protected void setupTouchListener()
    {
        //we create a new touch input that works as a proxy, but also supports new long press pan
        //controls.
        //We need to do this, rather than extend TouchInput, because MapController creates
        //a touch input in its constructor, and sets responders that call private methods within
        // it, such as nativeHandleFlingGesture(mapPointer, posX, posY, velocityX, velocityY);
        touchInput = new MyTouchInput(context, getTouchInput());
    }

    public boolean handleGesture(MapView mapView, MotionEvent ev) {
        return touchInput.onTouch(mapView, ev);
    }


    /**
     * Set a responder for long press gestures, including long press pan
     *
     * @param responder LongPressResponder to call
     */
    public void setLongPressResponderExt(final MyTouchInput.LongPressResponder responder) {
        touchInput.setLongPressResponder(new MyTouchInput.LongPressResponder() {
            @Override
            public void onLongPress(float x, float y) {
                if (responder != null) {
                    responder.onLongPress(x, y);
                }
            }

            @Override
            public boolean onLongPressPan(float x1, float x2, float x3, float x4) {
                if (responder != null) {
                    return responder.onLongPressPan(x1, x2, x3, x4);
                }
                return false;
            }

            @Override
            public void onLongPressUp(float x, float y) {
                if (responder != null) {
                    responder.onLongPressUp(x, y);
                }
            }
        });
    }


}
