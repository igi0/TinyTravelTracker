/** 
    Copyright 2024 Igor Calì <igor.cali0@gmail.com>

    This file is part of Open Travel Tracker.

    Open Travel Tracker is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Open Travel Tracker is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Open Travel Tracker.  If not, see <http://www.gnu.org/licenses/>.

*/
package com.igisw.openlocationtracker;

import android.Manifest;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.DataSetObserver;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.text.method.LinkMovementMethod;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.ViewTreeObserver.OnGlobalLayoutListener;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.TranslateAnimation;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ZoomControls;

import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.igisw.openlocationtracker.AndroidPreferenceSet.AndroidPreferences;
import com.igisw.openlocationtracker.GTG.GTGEvent;
import com.igisw.openlocationtracker.GTG.GTGEventListener;
import com.igisw.openlocationtracker.TimeView.Listener;
import com.igisw.openlocationtracker.ToolTipFragment.UserAction;
import com.mapzen.tangram.CameraPosition;
import com.rareventure.gps2.reviewer.map.sas.TimeRange;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.Objects;
import java.util.TimeZone;

//import com.igisw.openlocationtracker.TrialExpiredActivity;

//TODO 4: Satellite, street view??, traffic
public class OsmMapGpsTrailerReviewerMapActivity extends ProgressDialogActivity implements OnClickListener,  Listener,
GTGEventListener
{
	private ImageButton menuButton;
	private GpsLocationOverlay locationOverlay;
	private LocationManager locationManager;
//	private boolean userDoesntWantGpsOn;

	public void setupLocationUpdates(GpsLocationOverlay gpsLocationOverlay) {
		//the user may have disabled us from reading location data
		if(ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
				== PackageManager.PERMISSION_GRANTED)
		{
			/*
			Criteria criteria = new Criteria();
			criteria.setSpeedRequired(false);
			criteria.setAccuracy(Criteria.ACCURACY_FINE);
			criteria.setAltitudeRequired(false);
			criteria.setBearingRequired(false);
			criteria.setCostAllowed(false);

			String locationProviderName = locationManager.getBestProvider(criteria, true);
			*/
			locationManager.requestLocationUpdates(/*locationProviderName*/LocationManager.GPS_PROVIDER, 0, 0, gpsLocationOverlay, getMainLooper());
		}
	}

	public void removeLocationUpdates(GpsLocationOverlay gpsLocationOverlay) {
		locationManager.removeUpdates(gpsLocationOverlay);
	}


	private enum SasPanelState { GONE, TAB, FULL
	
	}
	
	private SasPanelState sasPanelState = SasPanelState.GONE;
	private TranslateAnimation slideSasFullToTab;
	private TranslateAnimation slideSasTabToFull;
	private TranslateAnimation slideSasFullToNone;
	private TranslateAnimation slideSasNoneToFull;
	private TranslateAnimation slideSasNoneToTab;
	private TranslateAnimation slideSasTabToNone;

	private TextView distanceView;

	@Override
	public void doOnCreate(Bundle savedInstanceState)
    {
        super.doOnCreate(savedInstanceState);

		checkPermissions();

		locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

		startForegroundService(new Intent(this, GpsTrailerService.class));

		timeAndDateSdf = new SimpleDateFormat(getString(R.string.time_and_date_format), Locale.getDefault());
        
        GTG.cacheCreatorLock.registerReadingThread();
        try {
        	/* ttt_installer:remove_line */Log.d(GTG.TAG,"OsmMapGpsTrailerReviewerMapActivity.onCreate()");
	        
	        //sometimes onDestroy forgets to be called, so we need to check for this and cleanup after the last instance
	        if(reviewerStillRunning)
	        {
	        	Log.w(GTG.TAG,"OsmMapGpsTrailerReviewerMapActivity: onDestroy() forgot to be called!");
	        	cleanup();
	        }
	        
	        reviewerStillRunning = true;
	        
            setContentView(R.layout.osm_gps_trailer_reviewer);

			osmMapView = findViewById(R.id.osmmapview);
			osmMapView.onCreate(savedInstanceState);

			initUI();
            
    		ViewTreeObserver vto = this.findViewById(android.R.id.content).getViewTreeObserver(); 
    	    vto.addOnGlobalLayoutListener(new OnGlobalLayoutListener() { 
    	    @Override 
    	    public void onGlobalLayout() { 
    	    	initWithWorkingGetWidth();
    	    	findViewById(android.R.id.content).getViewTreeObserver().removeOnGlobalLayoutListener(this);
    	    	osmMapView.initAfterLayout();
    	    } 
    	    }); 
        }
        finally {
        	GTG.cacheCreatorLock.unregisterReadingThread();
        }
	}

	protected void initWithWorkingGetWidth() {
        slideSasFullToTab = new TranslateAnimation(0, sasPanelButton.getWidth() - sasPanel.getWidth(), 0, 0);
        slideSasTabToFull = new TranslateAnimation(sasPanelButton.getWidth() - sasPanel.getWidth(), 0, 0, 0);
        slideSasTabToNone = new TranslateAnimation(sasPanelButton.getWidth() - sasPanel.getWidth(), - sasPanel.getWidth(), 0, 0);
		slideSasFullToNone = new TranslateAnimation(0, -sasPanel.getWidth(), 0, 0);
		slideSasNoneToFull = new TranslateAnimation(-sasPanel.getWidth(), 0, 0, 0);
		slideSasNoneToTab = new TranslateAnimation(-sasPanel.getWidth(),
				sasPanelButton.getWidth() - sasPanel.getWidth(), 0, 0);
		
		slideSasFullToTab.setDuration(500);
		slideSasTabToFull.setDuration(500);
		slideSasTabToNone.setDuration(200);
		slideSasFullToNone.setDuration(500);
		slideSasNoneToFull.setDuration(500);
		slideSasNoneToTab.setDuration(200);
		
		osmMapView.setZoomCenter(osmMapView.getWidth()/2,
				findViewById(R.id.timeview_layout).getTop()/2);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		super.onCreateOptionsMenu(menu);
        
        return true;
	}

	
	
    @Override
	public boolean onPrepareOptionsMenu(Menu menu) {
    	menu.clear();
    	menu.add(R.string.settings);
		return super.onPrepareOptionsMenu(menu);
	}

	@Override
    public boolean onOptionsItemSelected(MenuItem item) {
        GTG.cacheCreatorLock.registerReadingThread();
        try {
        if(Objects.requireNonNull(item.getTitle()).equals(getText(R.string.settings)))
        {
        	startInternalActivity(new Intent(this, SettingsActivity.class));
            return true;
        }
        else if(item.getTitle().equals(getText(R.string.turn_off_photos)))
        {
        	prefs.showPhotos = false;
        	gpsTrailerOverlay.notifyViewNodesChanged();
        	
        	if(mediaGalleryFragment != null)
        		mediaGalleryFragment.finishBrowsing();
        	return true;
        }
        else if(item.getTitle().equals(getText(R.string.turn_on_photos)))
        {
        	prefs.showPhotos = true;
        	gpsTrailerOverlay.notifyViewNodesChanged();
        	return true;
        }
        else if(item.getTitle().equals(getText(R.string.help)))
        {
        	startInternalActivity(new Intent(this, ShowManual.class));
        	return true;
        }
        	
        return super.onOptionsItemSelected(item);
        }
        finally {
        	GTG.cacheCreatorLock.unregisterReadingThread();
        }
    }

//    private static final int DIALOG_DELETE_NAMED_LOCATION = 1;
//	private static final int INITIAL_SETUP_REQUEST = 0;
//	private static final long MAX_SANE_PERIOD_MS = Util.MS_PER_YEAR * 10;
	public final Runnable NOTIFY_HAS_DRAWN_RUNNABLE = this::notifyHasDrawn;
	
	private ImageView autoZoom;

	public GpsTrailerOverlay gpsTrailerOverlay;
//	private long minRecordedTimeMs;
//	private long maxRecordedTimeMs;
	public final static Preferences prefs = new Preferences();
	OsmMapView osmMapView;
	MapScaleWidget scaleWidget;
	private Dialog currentDialog;
	
	public ImageView panToLocation;
//	private boolean timeViewDisplayed;
	
	TimeView timeView;
	View datePicker;
	private OngoingProcessStatusFragment gtgStatus;
	ToolTipFragment toolTip;
	ZoomControls zoomControls;
	private InfoNoticeStatusFragment infoNotice;
	
//	private CheckBox selectedAreaAddLock;
	private View sasPanel;
	private ImageButton sasPanelButton;
//	private ImageButton selectedAreaView;
	//this is static because sometimes onDestroy from the last time this
	//activity was run isn't called. In this case we want to make sure to
	//be able to kill the threads before starting again
	// and it does create a completely new instance of this class
	private static SuperThreadManager superThreadManager;
	
	private static boolean reviewerStillRunning;
	
	
	/**
	 * Notifies the activity that the selected areas have been
	 * changed by the user.
	 * @param isSet true if there are any selected areas set after
	 *   the operation.
	 */
	public void notifySelectedAreasChanged(boolean isSet)
	{
		if(isSet)
		{
			if(sasPanelState == SasPanelState.GONE)
			{
				slideSas(SasPanelState.FULL);
			}
		}
		else
		{
			slideSas(SasPanelState.GONE);
		}
	}
	
	private void slideSas(final SasPanelState newState) {
		final TranslateAnimation anim;
		
		if(newState == SasPanelState.FULL)
		{
			if(sasPanelState == SasPanelState.GONE)
				anim = slideSasNoneToFull;
			else if (sasPanelState == SasPanelState.TAB)
			{
				anim = slideSasTabToFull;
			}
			else
				return; //nothing to do
		}
		else if(newState == SasPanelState.TAB)
		{
			if(sasPanelState == SasPanelState.GONE)
				anim = slideSasNoneToTab;
//				throw new IllegalStateException("not supported");
			else if (sasPanelState == SasPanelState.FULL)
				anim = slideSasFullToTab;
			else
				return; //nothing to do
		}
		else //if(newState == SasPanelState.GONE)
		{
			if(sasPanelState == SasPanelState.TAB)
				anim = slideSasTabToNone;
			else if (sasPanelState == SasPanelState.FULL)
				anim = slideSasFullToNone;
			else
				return; //nothing to do
		}
		
		if(sasPanelState == SasPanelState.TAB)
			sasPanel.findViewById(R.id.sas_main_panel).setVisibility(View.VISIBLE);
        sasPanel.startAnimation(anim);
        anim.setAnimationListener(new AnimationListener() {
			
			@Override
			public void onAnimationStart(Animation animation) {
			}
			
			@Override
			public void onAnimationRepeat(Animation animation) {
			}
			
			@Override
			public void onAnimationEnd(Animation animation) {
				if(newState == SasPanelState.TAB)
				{
					sasPanel.findViewById(R.id.sas_main_panel).setVisibility(View.GONE);
			        sasPanelButton.setBackgroundResource(R.drawable.sas_tab_out);
				}
				else
					sasPanelButton.setBackgroundResource(R.drawable.sas_tab_in);
				
				sasPanel.setVisibility(newState != SasPanelState.GONE ? View.VISIBLE : View.INVISIBLE);
				
				sasPanelState = newState;
			}
		});
        
	}
	


	protected void notifyHasDrawn() {
		
		TimeZoneTimeRow newTimeZone = GTG.tztSet.getTimeZoneCovering(gpsTrailerOverlay.closestToCenterTimeSec);
		
		timeView.updateTimeZone(newTimeZone);
		
		osmMapView.invalidate();
	}

	private void initUI()
	{
		TextView ty = findViewById(R.id.thankyou);
	    ty.setMovementMethod(LinkMovementMethod.getInstance());

        sasPanelButton = findViewById(R.id.sas_open_close_button);
        
        sasPanelButton.setOnClickListener(this);
		
        gtgStatus = (OngoingProcessStatusFragment) getSupportFragmentManager().
    	findFragmentById(R.id.status);

        toolTip = (ToolTipFragment) getSupportFragmentManager().
    	findFragmentById(R.id.tooltip);

        infoNotice = (InfoNoticeStatusFragment) getSupportFragmentManager().
    	findFragmentById(R.id.infoNotice);
    
        superThreadManager = new SuperThreadManager();
        SuperThread cpuThread = new SuperThread(superThreadManager);
        SuperThread fileIOThread = new SuperThread(superThreadManager);

        fileIOThread.start();
        cpuThread.start();

        zoomControls = this.findViewById(R.id.zoomControls);
        zoomControls.setOnZoomInClickListener(v -> {
			osmMapView.zoomIn();

			toolTip.setAction(UserAction.ZOOM_IN);
		});
        zoomControls.setOnZoomOutClickListener(v -> {
			osmMapView.zoomOut();

			toolTip.setAction(UserAction.ZOOM_OUT);
		});

		osmMapView.addOverlay(gpsTrailerOverlay = new GpsTrailerOverlay(this, cpuThread, osmMapView));
		osmMapView.addOverlay(locationOverlay = new GpsLocationOverlay(this));
        osmMapView.init(/*fileIOThread,*/ this);

        scaleWidget = this.findViewById(R.id.scaleWidget);
        
		osmMapView.setScaleWidget(scaleWidget);
		
        panToLocation = this.findViewById(R.id.pan_to_location);
        panToLocation.setOnClickListener(this);

        autoZoom = this.findViewById(R.id.AutoZoom);
        autoZoom.setOnClickListener(this);

		menuButton = findViewById(R.id.menu_button);
		menuButton.setOnClickListener(this);

		datePicker = findViewById(R.id.date_picker);
		datePicker.setOnClickListener(this);

        timeView = findViewById(R.id.timeview);
		
		timeView.setListener(this);
		
		timeView.setActivity(this);
		
		sasPanel = findViewById(R.id.sas_panel);
		sasPanel.setVisibility(View.INVISIBLE);
		
		distanceView = findViewById(R.id.dist_traveled);
		updateDistanceView(-1);
		
		initSasPanel();
	}

	
	private SimpleDateFormat timeAndDateSdf;
	private MediaGalleryFragment mediaGalleryFragment;
	private boolean locationKnown;
	
	
	private void initSasPanel() {
		ListView sasPanelList = findViewById(R.id.sas_grid);

		sasPanelList.setOnItemClickListener((parent, view, position, id) -> {
            //note that getTimeRange reuses the same tr instance, so we have to be careful
            // when we call it twice
            TimeRange tr = gpsTrailerOverlay.sas.getTimeRange(position-1);

            int currStartSec = tr.startTimeSec;
            int currEndSec = tr.endTimeSec;

            if(currEndSec - currStartSec < timeView.getMinSelectableTimeSec())
            {
                currEndSec = currStartSec + timeView.getMinSelectableTimeSec();
            }

            setStartAndEndTimeSec(currStartSec, currEndSec);
            updateTimeViewTime();

        });
		
		sasPanelList.setAdapter(new ListAdapter() {
			
			@Override
			public void unregisterDataSetObserver(DataSetObserver observer) {
				gpsTrailerOverlay.sas.unregisterDataSetObserver(observer);
				
			}
			
			@Override
			public void registerDataSetObserver(DataSetObserver observer) {
				gpsTrailerOverlay.sas.registerDataSetObserver(observer);
			}
			
			@Override
			public boolean isEmpty() {
				return gpsTrailerOverlay.sas.isEmpty();
			}
			
			@Override
			public boolean hasStableIds() {
				return false; //because we might merge time views
			}
			
			@Override
			public int getViewTypeCount() {
				return 2;
			}
			
			public View getView(int position, View convertView, ViewGroup parent) {
				if(convertView == null)
				{
					if(position == 0)
						convertView = getLayoutInflater().
							inflate(R.layout.selected_area_info_top_row, null);
					else
						convertView = getLayoutInflater().
							inflate(R.layout.selected_area_info_row, null);
				}

				if(position == 0)
				{
					((TextView)convertView.findViewById(R.id.totalTime)).setText(Util.convertMsToText(getApplicationContext(), gpsTrailerOverlay.sas.getTotalTimeSecs()* 1000L));
					((TextView)convertView.findViewById(R.id.totalDist)).setText(MapScaleWidget.calcLabelForLength(gpsTrailerOverlay.sas.getTotalDistM(),
							GTG.prefs.useMetric));
					((TextView)convertView.findViewById(R.id.timesInArea)).setText(String.valueOf(gpsTrailerOverlay.sas.getTimesInArea()));
					
					TimeZone tz = gpsTrailerOverlay.sas.timeZone;
					
					if(tz == null || tz.hasSameRules(Util.getCurrTimeZone()))
					{
						convertView.findViewById(R.id.timeZoneTableRow).setVisibility(View.GONE);
					}
					else
					{
						convertView.findViewById(R.id.timeZoneTableRow).setVisibility(View.VISIBLE);
						((TextView)convertView.findViewById(R.id.timezone)).setText(tz.getDisplayName());
					}
					
					return convertView;
				}
				
				TimeRange tr = gpsTrailerOverlay.sas.getTimeRange(position-1);
				
				timeAndDateSdf.setTimeZone(gpsTrailerOverlay.sas.timeZone);
				
				String startText = timeAndDateSdf.format(new Date(tr.startTimeSec * 1000L));
				String endText = timeAndDateSdf.format(new Date(tr.endTimeSec * 1000L));
				
				((TextView)convertView.findViewById(R.id.startTime)).setText(startText);
				((TextView)convertView.findViewById(R.id.endTime)).setText(endText);
				
				String distText;
				if(tr.dist == -1)
					distText = "--";
				else
				{
					distText = MapScaleWidget.calcLabelForLength(tr.dist, GTG.prefs.useMetric);
				}
				
				((TextView)convertView.findViewById(R.id.distance)).setText(distText);
				
				//this fixes a bug where sometimes if the last row is deleted and read, it isn't
				//laid out again, causing the date/times to be cut off
				convertView.requestLayout();
				
//				((TextView)convertView.findViewById(R.id.timeLength)).setText("a certain amount of time");
				
				return convertView;
			}
			
			@Override
			public int getItemViewType(int position) {
				if(position == 0)
					return 0;
				return 1;
			}
			
			@Override
			public long getItemId(int position) {
				if(position == 0)
					return Long.MIN_VALUE;
				
				TimeRange tr = gpsTrailerOverlay.sas.getTimeRange(position-1);

				return tr.fullRangeStartSec;
			}
			
			@Override
			public Object getItem(int position) {
				if(position == 0)
					return null;
				return gpsTrailerOverlay.sas.getTimeRange(position-1);
			}
			
			@Override
			public int getCount() {
				int count = gpsTrailerOverlay.sas.getTimeRangeCount();
//				Log.d(GTG.TAG,"item count is "+count);
				
				if(count >= 1)
					return count+1;
				return 0;
			}
			
			@Override
			public boolean isEnabled(int position) {
				return position != 0;
			}
			
			@Override
			public boolean areAllItemsEnabled() {
				return false;
			}
		});
	}

//    protected Dialog onCreateDialog(int id) {
//    	if(currentDialog != null)
//    		currentDialog.dismiss();
//    	
//        switch (id) {
//
//        case DIALOG_DELETE_NAMED_LOCATION:
//            this.currentDialog = new AlertDialog.Builder(this)
//                .setTitle(Util.varReplace(this.getString(R.string.delete_named_location), 
//                		"item", gpsClickData.name))
//                .setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
//                    public void onClick(DialogInterface dialog, int whichButton) {
//                    	deleteNamedLocation();
//                    }
//
//                })
//                .setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {
//                    public void onClick(DialogInterface dialog, int whichButton) {
//                    }
//                })
//                .create();
//        }
//        
//        return null;
//    }
    
	//TODO 3: have a clearer dial. So the dial darkens only when you drape your finger over the screen
	// otherwise it is clear, or minimized so that it doesn't take up too much screen real estate.
	
	
	@Override
	public void onClick(View v) {
        GTG.cacheCreatorLock.registerReadingThread();
        try {
			if(v == panToLocation)
			{
				if(locationKnown)
				{
					osmMapView.panTo(locationOverlay.getLastLoc());
					toolTip.setAction(ToolTipFragment.UserAction.PAN_TO_LOCATION_BUTTON);
				}
				else
				{
					Toast.makeText(OsmMapGpsTrailerReviewerMapActivity.this, R.string.location_unknown, Toast.LENGTH_SHORT).show();
				}
			}
			else if(v == datePicker)
			{
				startInternalActivity(new Intent(this, EnterFromDateToToDateActivity.class));
				toolTip.setAction(ToolTipFragment.UserAction.DATE_PICKER);

			}
			else if(v == autoZoom)
			{
				doAutozoom(true);

				toolTip.setAction(ToolTipFragment.UserAction.AUTOZOOM_BUTTON);
			}
			else if(v == menuButton)
			{
				startInternalActivity(new Intent(this, SettingsActivity.class));
			}
			else if(v == sasPanelButton)
			{
				if(sasPanelState == SasPanelState.FULL)
					slideSas(SasPanelState.TAB);
				else
					slideSas(SasPanelState.FULL);
			}
        }
        finally {
        	GTG.cacheCreatorLock.unregisterReadingThread();
        }

	}

	private void doAutozoom(boolean showToastOnFailure) {
		AreaPanelSpaceTimeBox stBox;
		
		gpsTrailerOverlay.sas.rwtm.registerReadingThread();
		
		try {
	    	stBox = GTG.apCache.getAutoZoomArea(prefs.currTimePosSec, 
	    			prefs.currTimePosSec + prefs.currTimePeriodSec, gpsTrailerOverlay.sas.getResultPaths());

			if(stBox == null)
			{
				if(showToastOnFailure)
					Toast.makeText(OsmMapGpsTrailerReviewerMapActivity.this, getText(R.string.auto_zoom_no_data), Toast.LENGTH_SHORT).show();
				return; 
			}
			
			osmMapView.panAndZoom(stBox.minX,stBox.minY,stBox.maxX,stBox.maxY);
		}
		finally
		{
			gpsTrailerOverlay.sas.rwtm.unregisterReadingThread();
		}
	}
	
	/*
	private Float determineSpeed(long lastId, Integer lastLatM, Integer lastLonM, Long lastTime,
			Integer latm, Integer lonm, Long time) {
		Cursor c = null;
		
		try {
			if(lastLatM == null)
			{
				c = GTG.db.rawQuery(
						"select lat_md, lon_md, time from GPS_LOCATION_TIME where _id = ?", new String[] { String.valueOf(lastId) });
				if(!c.moveToNext()) //if we can't find the previous id
					return null;
				lastLatM = c.getInt(0);
				lastLonM = c.getInt(1);
				lastTime = c.getLong(2);
			}
			if(latm == null)
			{
				c = GTG.db.rawQuery(
						"select lat_md, lon_md, time from GPS_LOCATION_TIME where _id = ?", 
						new String[] { String.valueOf(lastId + 1) });
				if(!c.moveToNext()) //if we can't find the previous id
					return null;
				latm = c.getInt(0);
				lonm = c.getInt(1);
				time = c.getLong(2);
			}
			
			Location l1 = new Location("me");
			l1.setLatitude(lastLatM/1000000f);
			l1.setLongitude(lastLonM/1000000f);
			
			Location l2 = new Location("me");
			l2.setLatitude(latm/1000000f);
			l2.setLongitude(lonm/1000000f);
			
			long period = time - lastTime;
			if(period <=0) period = 1;
			
			return l1.distanceTo(l2) / period;
		}
		finally {
			DbUtil.closeCursors(c);
		}
	}
	*/

	public void redrawMap() {
		osmMapView.redrawMap();
	}		

	public static class Preferences implements AndroidPreferences
	{

//		public long minTimePeriodMs = 300*1000;

		public boolean showPhotos = true;
		
		/**
		 * The amount of scaling of a panel, in terms of zoom level. ie. 2 would equal 2x, and would mean
		 * the smallest tile would be spread out over twice the area in x and y.
		 */
		public int panelScale = 2;

//		public long lastCheckedGpsLocationIdForUIManager = Long.MIN_VALUE;
		/**
		 * The amount of padding
		 */
//		public float zoomPaddingPerc = .2f;
		public final int [] allColorRanges =
			new int [] { 0xff000000, 0xff808080, 0xffff0000, 0xffff8000, 0xffffff00, 0xff80ff00, 0xff00ff00, 
				0xff00ff80,
        		0xff00ffff, 0xff0080ff, 0xff0000ff, 0xffffffff};
		
		//to change default colors, make sure to also update selectedColorRangesBitmap
		public int [] colorRange = 
			new int [] { 0xffff0000, 0xffff8000, 0xffffff00, 0xff80ff00, 0xff00ff00, 
				0xff00ff80,
        		0xff00ffff, 0xff0080ff, 0xff0000ff };

		public int currTimePosSec = (int)(System.currentTimeMillis()/ 1000L) - Util.SECONDS_IN_DAY, currTimePeriodSec = Util.SECONDS_IN_DAY*3;
	
		
		/**
		 * Last location of screen when TTT was last visited.
		 */
		public double lastLon, lastLat;
		public float lastZoom;

		/**
		 * Boolean map of color ranges that are in use in allColorRanges
		 */
		//no black, grey or white
		public int selectedColorRangesBitmap = ((1<<12)-1)&(~(2|1|(1<<11)));

		public boolean enableToolTips = true;

		public void updateColorRangeBitmap(int newColorRangesBitmap) {
			int [] newColorRange = new int[allColorRanges.length];
			
			int ncrIndex = 0;
			
			for(int i = 0; i < allColorRanges.length; i++)
			{
				if(((1<<i)&newColorRangesBitmap) != 0)
					newColorRange[ncrIndex++] = allColorRanges[i];
			}
			
			colorRange = new int[ncrIndex];
			System.arraycopy(newColorRange, 0, colorRange, 0, ncrIndex);
			
			this.selectedColorRangesBitmap = newColorRangesBitmap;
		}
		
	}
	
	

	/*
	public void editUserLocation(GpsClickData gpsClickData) {
        GTG.cacheCreatorLock.registerReadingThread();
        try {

			turnOnSpeechBubble();
        }
        finally {
        	GTG.cacheCreatorLock.unregisterReadingThread();
        }
	}
	*/

	/*
	public static class GpsClickData
	{
		public boolean isEdit;

		public int id;
		
		public int lonm;

		public int latm;

		public double radius;

		private String name;

	 */

		/*
		public static GpsClickData createUserLocation(int latm, int lonm, double radius)
		{
			GpsClickData gcd = new GpsClickData();
			
			gcd.id = Integer.MIN_VALUE;
			gcd.name = "";
			gcd.latm = latm;
			gcd.lonm = lonm;
			
			return gcd;
		}

		public static GpsClickData editUserLocation(int id, String name, int latm, int lonm) {
			GpsClickData gcd = new GpsClickData();
			gcd.id = id;
			gcd.name = name;
			gcd.lonm = lonm;
			gcd.latm = latm;
			gcd.isEdit = true;
			
			return gcd;
		}
		*/
	/*
	}
	*/


	
	@Override
	public void doOnResume() {
		super.doOnResume();

		datePicker.setVisibility(View.VISIBLE);
		timeView.setVisibility(View.VISIBLE);
		toolTip.requireView().setVisibility(View.VISIBLE);
		
		toolTip.setEnabled(prefs.enableToolTips);
		
		scaleWidget.setUnitsToMetric(GTG.prefs.useMetric);
		
        GTG.cacheCreatorLock.registerReadingThread();
        try {
		
		GTG.reviewerMapResumeId++;
		
		//PERF: we should only notify media dirty when we didn't go
		// to settings or select time/date
		GTG.cacheCreator.setGtum(this);
		GTG.cacheCreator.notifyMediaDirty();
		
		GTG.mediaLocTimeMap.notifyResume();
		
		//note, this is with the gtg cache creator because drawer
		//won't pause while its holding onto a cache creator lock
		superThreadManager.resumeAllSuperThreads();
		
		osmMapView.onResume();
		
		// we do this so that if the user deleted a picture it will be 
		//removed from the display.. it may be null if we are actually going to the start
		//screen instead, or we haven't drawn at all
		if(gpsTrailerOverlay != null)
		{
			gpsTrailerOverlay.notifyViewNodesChanged();
			
			//just in case it was changed
			//TODO 1.5 FIXME
//			gpsTrailerOverlay.updateForColorRangeChange();
		}
		
		
		if(GTG.lastSuccessfulAction == GTG.GTGAction.SET_FROM_AND_TO_DATES)
		{
			updateTimeViewTime();
			doAutozoom(false);
			GTG.lastSuccessfulAction = null;
		}
		

		updateTimeViewMinMaxTime();
        
		timeView.ovalDrawer.updateColorRange();
		timeView.invalidate();
		
		
        }
        finally {
        	GTG.cacheCreatorLock.unregisterReadingThread();
        }
        
        //check all events and update accordingly
		updateStatusFromGTGEvent(null);

		GTG.addGTGEventListener(this);
		
		
   }

	/**
	 * Updates the time view min and max time. If minTime != maxTime (ie there are
	 * at least one point cached.) We also update the selected time view to be within
	 * the min max range
	 */
	private void updateTimeViewMinMaxTime() {
		GTG.cacheCreatorLock.registerReadingThread();
        try {
        	
        	if(GTG.cacheCreator.minTimeSec != GTG.cacheCreator.maxTimeSec)
        	{
	        	//if for whatever reason the selected times were outside of min/max
	        	//we need to update them
	        	//note that we updateTimeViewTime() here even though the TimeView may not be laid out and this
	        	//may not have any effect, because if the page is paused and then resumed, the normal way
	        	// of calling updateTimeViewTime() (on layout of the TimeView) will not happen
	        	boolean timeChanged = false;
	        	
	    		if(OsmMapGpsTrailerReviewerMapActivity.prefs.currTimePosSec >  GTG.cacheCreator.maxTimeSec)
	    		{
	    			OsmMapGpsTrailerReviewerMapActivity.prefs.currTimePosSec = GTG.cacheCreator.maxTimeSec;
	    			timeChanged = true;
	    		}
	    		if(prefs.currTimePosSec + prefs.currTimePeriodSec < GTG.cacheCreator.minTimeSec)
	    		{
	    			prefs.currTimePosSec -= GTG.cacheCreator.minTimeSec - (prefs.currTimePosSec + prefs.currTimePeriodSec);
	    			timeChanged = true;
	    		}
	    		
	    		if(timeChanged)
	    			updateTimeViewTime();
	    		
        	}

        	timeView.setMinMaxTime(GTG.cacheCreator.minTimeSec, GTG.cacheCreator.maxTimeSec);
        }
        finally {
        	GTG.cacheCreatorLock.unregisterReadingThread();
        }
	}

	@Override
	public void doOnPause(boolean doOnResumeCalled) {
		super.doOnPause(doOnResumeCalled);

		GTG.removeGTGEventListener(this);
		
        GTG.cacheCreatorLock.registerReadingThread();
        try {

		if(currentDialog != null)
		{
			currentDialog.dismiss();
			currentDialog = null;
		}

		if(osmMapView != null) {
			osmMapView.onPause();

			//sometimes on pause gets called when we're not fully started up
			if(osmMapView.getMapController() != null) {
				CameraPosition cp = osmMapView.getMapController().getCameraPosition();

				prefs.lastLat = cp.latitude;
				prefs.lastLon = cp.longitude;
				prefs.lastZoom = cp.zoom;

				GTG.runBackgroundTask(() -> GTG.savePreferences(OsmMapGpsTrailerReviewerMapActivity.this));
			}
		}

		if(GTG.cacheCreator != null)
			GTG.cacheCreator.setGtum(null);

		if(superThreadManager != null)
			superThreadManager.pauseAllSuperThreads();
        }
        finally {
        	GTG.cacheCreatorLock.unregisterReadingThread();
        }
        
	}

	public void onDestroy() {
//		/* ttt_installer:remove_line */Log.d(GTG.TAG,"OsmMapGpsTrailerReviewerMapActivity.onDestory() start");
        
		super.onDestroy();
		if(osmMapView != null)
			osmMapView.onDestroy();

        GTG.cacheCreatorLock.registerReadingThread();
        try {

			cleanup();
//			/* ttt_installer:remove_line */Log.d(GTG.TAG,"OsmMapGpsTrailerReviewerMapActivity.onDestory() end");
        }
        finally {
        	GTG.cacheCreatorLock.unregisterReadingThread();
        }
	}

	@Override
	public void onLowMemory() {
		super.onLowMemory();
		if(osmMapView != null)
			osmMapView.onLowMemory();
	}

	private void cleanup()
	{
		if(superThreadManager != null)
			superThreadManager.shutdownAllSuperThreads();

		//this happens when finish() has been called (or the system wants us dead), so
		//clear out the database and crypt instance, to force user to reenter password
		
		//co:we don't want to close and reopen the database because this takes forever
//		if(GTG.db != null)
//			GTG.db.close();
//		GTG.db = null;

		//TODO 2.5 why is this null sometimes?
		if(gpsTrailerOverlay != null)
			gpsTrailerOverlay.shutdown();
		reviewerStillRunning = false;
	}
	

	@Override
	public void notifyTimeViewChange() {
        GTG.cacheCreatorLock.registerReadingThread();
        try {
			recalculateStartAndEndTimeFromTimeView();
			redrawMap();
			
			if(GTG.apCache.hasGpsPoints())
				toolTip.setAction(UserAction.TIME_VIEW_CHANGE);
        }
        finally {
        	GTG.cacheCreatorLock.unregisterReadingThread();
        }
	}
	
	/**
	 * May be called from another thread
	 */
	public void notifyMediaChanged()
	{
		runOnUiThread(() -> {
			if(gpsTrailerOverlay != null)
				//PERF we could have a special method for pictures, because this
				//updates distance, too
				gpsTrailerOverlay.notifyViewNodesChanged();
		});
	}
	
	@Override
	public void notifyTimeViewReady() {
		updateTimeViewTime();
	}

	/**
	 *  Should be called only by ui thread
	 */
	public void notifyMaxTimeChanged() {
		//note that this also updates the selected time if out of range. The reason that may happen,
		// is that we just did a restore (or for some reason the cache was deleted), and then
		// some points were finally cached (if there are no points cached then the selected times
		// are not updated
		updateTimeViewMinMaxTime();
	}
	
	private void updateTimeViewTime() {
        GTG.cacheCreatorLock.registerReadingThread();
        try {
        	
        	if(timeView.getWidth() != 0)
				timeView.setSelectedStartAndEndTime(prefs.currTimePosSec, 
						prefs.currTimePosSec + prefs.currTimePeriodSec
						);
			
			//time view may adjust the end time if it is below the minimum (or above the maximum) that
			//it can display
			recalculateStartAndEndTimeFromTimeView();
	    }
	    finally {
	    	GTG.cacheCreatorLock.unregisterReadingThread();
	    }
	}

	private void recalculateStartAndEndTimeFromTimeView() {
		setStartAndEndTimeSec(timeView.selectedTimeStart, timeView.selectedTimeEnd );
	}

		
	public static synchronized void setStartAndEndTimeSec(int startTimeSec, int endTimeSec) {
		prefs.currTimePosSec = startTimeSec;
		prefs.currTimePeriodSec = endTimeSec - startTimeSec;
	}

	public void notifyLocationKnown() {
        GTG.cacheCreatorLock.registerReadingThread();
        try {
        	panToLocation.setBackgroundResource(R.drawable.pan_to_location);
        	locationKnown = true;
        }
        finally {
        	GTG.cacheCreatorLock.unregisterReadingThread();
        }
	}
	
	
	public enum OngoingProcessEnum
	{
		PROCESS_GPS_POINTS(R.string.process_gps_points),
		DRAW_POINTS(R.string.drawing_points), LOADING_MEDIA(R.string.loading_media);
		
		final Integer resourceId;
		
		OngoingProcessEnum(int resourceId)
		{
			this.resourceId = resourceId;
		}
	}
	
	public void notifyProcessing(OngoingProcessEnum ope, String text) {
		gtgStatus.registerProcess(ope.resourceId, text, null, null);
	}

	public void notifyProcessing(OngoingProcessEnum ope) {
		gtgStatus.registerProcess(ope.resourceId, getText(ope.resourceId), null, null);
	}

	public void notifyDoneProcessing(OngoingProcessEnum ope) {
		gtgStatus.unregisterProcess(ope.resourceId);
	}

	@Override
	public boolean onGTGEvent(final GTGEvent event) {
		return updateStatusFromGTGEvent(event);
	}
	
	/**
	 * @param event if event is specified, only it will be updated, otherwise
	 *   all events are checked if they are on or off
	 */
	private boolean updateStatusFromGTGEvent(final GTGEvent event) {
		if (event == null || event == GTGEvent.ERROR_LOW_FREE_SPACE) {
			runOnUiThread(() -> {
				if (GTGEvent.ERROR_LOW_FREE_SPACE.isOn)
					infoNotice
							.registerProcess(
									InfoNoticeStatusFragment.LOW_FREE_SPACE,
									getString(R.string.error_reviewer_low_free_space),
									null, () -> {
										AlertDialog.Builder alert = new AlertDialog.Builder(
												OsmMapGpsTrailerReviewerMapActivity.this);
										alert.setMessage(getText(R.string.error_reviewer_low_free_space_help));
										alert.setPositiveButton(
												R.string.ok,
												(dialog, whichButton) -> exitFromApp());
										alert.show();

									});
				else
					infoNotice
							.unregisterProcess(InfoNoticeStatusFragment.LOW_FREE_SPACE);
			});

			return false;
		}
		if (event == GTGEvent.ERROR_SDCARD_NOT_MOUNTED) {
			runOnUiThread(() -> startInternalActivity(new Intent(OsmMapGpsTrailerReviewerMapActivity.this,
					FatalErrorActivity.class).putExtra(
					FatalErrorActivity.MESSAGE_RESOURCE_ID,
					R.string.error_reviewer_sdcard_not_mounted)));
			return false;
		}
		if (event == GTGEvent.PROCESSING_GPS_POINTS) {
			runOnUiThread(() -> {
				if (GTGEvent.PROCESSING_GPS_POINTS.isOn) {
					timeAndDateSdf.setTimeZone(Util.getCurrTimeZone());

					long[] currDateAndExpectedDate = (long[]) event.obj;

					//this can sometimes be null because (I believe) that onGTGEvent
					// and offGTGEvent are called in rapid succession, and offGTGEvent
					// destroys the object associated to the event before we can
					// process the onGTGEvent message
					//TODO 2.5 fix the GTGEvent.obj race condition
					if (currDateAndExpectedDate != null)
						notifyProcessing(
								OngoingProcessEnum.PROCESS_GPS_POINTS,
								String.format(
										getString(R.string.process_gps_points_fmt),
										timeAndDateSdf
												.format(new Date(
														currDateAndExpectedDate[0]))));
				}
				else
					notifyDoneProcessing(OngoingProcessEnum.PROCESS_GPS_POINTS);
			});
		}

		if (event == GTGEvent.LOADING_MEDIA) {
			runOnUiThread(() -> {
				if (GTGEvent.LOADING_MEDIA.isOn)
					notifyProcessing(OngoingProcessEnum.LOADING_MEDIA);
				else
					notifyDoneProcessing(OngoingProcessEnum.LOADING_MEDIA);
			});
		}

		return false;
	}

	@Override
	public void offGTGEvent(GTGEvent event) {
		updateStatusFromGTGEvent(event);
	}

	public void notifyPathsChanged() {
		if(gpsTrailerOverlay != null)
			gpsTrailerOverlay.notifyPathsChanged();
	}

	public void notifyDistUpdated(final double distance) {
		runOnUiThread(() -> updateDistanceView(distance));
	}

	protected void updateDistanceView(double distance) {
		distanceView.setText(String.format(getText(R.string.distance_traveled).toString(),
				distance == -1 ? "--" : 
					MapScaleWidget.calcLabelForLength(distance, GTG.prefs.useMetric)));
				
		
	}

	public void registerMediaGalleryFragment(
			MediaGalleryFragment mediaGalleryFragment) {
		this.mediaGalleryFragment = mediaGalleryFragment;
		
	}

	public void unregisterMediaGalleryFragment(
			MediaGalleryFragment mediaGalleryFragment2) {
		this.mediaGalleryFragment = null;
		
	}
	
	@Override
	public int getRequirements() {
		return GTG.REQUIREMENTS_FULL_PASSWORD_PROTECTED_UI;
	}

	public void checkPermissions(){
		if (ActivityCompat.checkSelfPermission(
				this,
				Manifest.permission.ACCESS_FINE_LOCATION
		) != PackageManager.PERMISSION_GRANTED
		) {
				requestLocationPermission();

				// TODO: remove Looper;
				new Handler(Looper.getMainLooper()).postDelayed(this::requestBackgroundLocationPermission, 5000);

			}
		else {
			if (ActivityCompat.checkSelfPermission(
					this,
					Manifest.permission.ACCESS_BACKGROUND_LOCATION
			) != PackageManager.PERMISSION_GRANTED
			) {
				requestBackgroundLocationPermission();
			}
		}

		boolean WriteStoragePermission = (ContextCompat.checkSelfPermission(this,
				Manifest.permission.WRITE_EXTERNAL_STORAGE)
				== PackageManager.PERMISSION_GRANTED) ||
				(ContextCompat.checkSelfPermission(this,
						Manifest.permission.WRITE_EXTERNAL_STORAGE)
						== PackageManager.PERMISSION_GRANTED);
		if(! WriteStoragePermission){
			int ASK_WRITESTORAGE_PERMISSION = 102;
			ActivityCompat.requestPermissions(this,
					new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE,
							Manifest.permission.WRITE_EXTERNAL_STORAGE},
					ASK_WRITESTORAGE_PERMISSION);

		}

		boolean ReadStoragePermission = (ContextCompat.checkSelfPermission(this,
				Manifest.permission.READ_EXTERNAL_STORAGE)
				== PackageManager.PERMISSION_GRANTED) ||
				(ContextCompat.checkSelfPermission(this,
						Manifest.permission.READ_EXTERNAL_STORAGE)
						== PackageManager.PERMISSION_GRANTED);
		if(! ReadStoragePermission){
			int ASK_READSTORAGE_PERMISSION = 101;
			ActivityCompat.requestPermissions(this,
					new String[]{Manifest.permission.READ_EXTERNAL_STORAGE,
							Manifest.permission.READ_EXTERNAL_STORAGE},
					ASK_READSTORAGE_PERMISSION);

		}

		boolean WakeLockPermission = (ContextCompat.checkSelfPermission(this,
				Manifest.permission.WAKE_LOCK)
				== PackageManager.PERMISSION_GRANTED) ||
				(ContextCompat.checkSelfPermission(this,
						Manifest.permission.WAKE_LOCK)
						== PackageManager.PERMISSION_GRANTED);
		if(! WakeLockPermission) {
			int ASK_WAKELOCK_PERMISSION = 104;
			ActivityCompat.requestPermissions(this,
					new String[]{Manifest.permission.WAKE_LOCK,
							Manifest.permission.WAKE_LOCK},
					ASK_WAKELOCK_PERMISSION);

		}
	}

	private void requestLocationPermission() {
        int MY_PERMISSIONS_REQUEST_LOCATION = 99;
        ActivityCompat.requestPermissions(
			this,
			new String[]{
					Manifest.permission.ACCESS_FINE_LOCATION
			},
                MY_PERMISSIONS_REQUEST_LOCATION
		);
	}

	private void requestBackgroundLocationPermission() {
		int MY_PERMISSIONS_REQUEST_BACKGROUND_LOCATION = 66;
		ActivityCompat.requestPermissions(
				this,
				new String[]{
						Manifest.permission.ACCESS_BACKGROUND_LOCATION
				},
				MY_PERMISSIONS_REQUEST_BACKGROUND_LOCATION
		);
	}

}
